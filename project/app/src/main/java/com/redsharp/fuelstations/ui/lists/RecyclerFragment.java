package com.redsharp.fuelstations.ui.lists;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.redsharp.fuelstations.R;

public class RecyclerFragment extends Fragment {

    private RecyclerView recyclerView;

    private StationAdapter stationAdapter;
    private RefuelAdapter refuelAdapter;

    public RecyclerFragment(StationAdapter adapter) {
        this.stationAdapter = adapter;
    }

    public RecyclerFragment(RefuelAdapter adapter) {
        this.refuelAdapter = adapter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.recycler_view_fragment, container, false);
        recyclerView = root.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        ItemTouchHelper.Callback callback;
        if (stationAdapter != null) {
            recyclerView.setAdapter(stationAdapter);
            callback = new ItemTouchHelperCallback(stationAdapter);
        } else {
            recyclerView.setAdapter(refuelAdapter);
            callback = new ItemTouchHelperCallback(refuelAdapter);
        }
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
        return root;
    }
}
