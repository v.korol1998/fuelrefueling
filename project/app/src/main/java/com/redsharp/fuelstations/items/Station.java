package com.redsharp.fuelstations.items;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Station {

    @PrimaryKey (autoGenerate = true)
    public long id;

    public String name;

    public String address;

    public Station() {
    }

    public Station(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
