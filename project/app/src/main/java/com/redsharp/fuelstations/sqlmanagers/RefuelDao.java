package com.redsharp.fuelstations.sqlmanagers;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.redsharp.fuelstations.items.Refuel;

import java.util.List;

@Dao
public interface RefuelDao {

    @Query("SELECT * FROM Refuel")
    LiveData<List<Refuel>> getAll();

    @Query("SELECT * FROM Refuel")
    List<Refuel> getList();

    @Query("SELECT * FROM Refuel WHERE id = :id")
    Refuel getById(long id);

    @Query("SELECT * FROM Refuel WHERE stationId = :stationId")
    List<Refuel> getRefuelsByStation(long stationId);

    @Query("DELETE FROM Refuel WHERE stationId = :stationId")
    void deleteByStation(long stationId);

    @Insert
    void insert(Refuel employee);

    @Update
    void update(Refuel employee);

    @Delete
    void delete(Refuel employee);
}
