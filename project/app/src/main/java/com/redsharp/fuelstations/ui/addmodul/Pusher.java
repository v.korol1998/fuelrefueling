package com.redsharp.fuelstations.ui.addmodul;

import com.redsharp.fuelstations.items.Refuel;
import com.redsharp.fuelstations.items.Station;

public interface Pusher {

    void push(Station station, Refuel refuel);

}
