package com.redsharp.fuelstations;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.room.Room;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.redsharp.fuelstations.items.Refuel;
import com.redsharp.fuelstations.items.Station;
import com.redsharp.fuelstations.sqlmanagers.AppDatabase;
import com.redsharp.fuelstations.sqlmanagers.RefuelDao;
import com.redsharp.fuelstations.sqlmanagers.StationDao;
import com.redsharp.fuelstations.ui.AddNewRefueling;
import com.redsharp.fuelstations.ui.main.SectionsPagerAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int ADD_RESULT_OK = 1;

    public static final String STATION = "station";
    public static final String REFUEL = "refuel";
    private AppDatabase appDatabase;
    private Gson gson = new Gson();
    private List<Station> oldStations;
    private List<Refuel> oldRefuels;

    private void compareStations(List<Station> newList){
        List<Station> tmp = new LinkedList<>(newList);
        for (int i = 0; i < oldStations.size(); i++) {
            Station o = oldStations.get(i);
            boolean remove = newList.remove(o);
            if(!remove){
                Intent service = new Intent(MainActivity.this, FireBaseService.class);
                service.putExtra(FireBaseService.Actions.ACTION, FireBaseService.Actions.DELETE);
                service.putExtra("station", gson.toJson(o));
                startService(service);
            }
        }
        for (int i = 0; i < newList.size(); i++) {
            Intent service = new Intent(MainActivity.this, FireBaseService.class);
            service.putExtra(FireBaseService.Actions.ACTION, FireBaseService.Actions.ADD);
            service.putExtra("station", gson.toJson(newList.get(i)));
            startService(service);
        }
        oldStations = tmp;
    }

    private void compareRefuels(List<Refuel> newList){
        List<Refuel> tmp = new LinkedList<>(newList);
        for (int i = 0; i < oldRefuels.size(); i++) {
            Refuel o = oldRefuels.get(i);
            boolean remove = newList.remove(o);
            if(!remove){
                Intent service = new Intent(MainActivity.this, FireBaseService.class);
                service.putExtra(FireBaseService.Actions.ACTION, FireBaseService.Actions.DELETE);
                service.putExtra("refuel", gson.toJson(o));
                startService(service);
            }
//            else {
//                Intent service = new Intent(MainActivity.this, FireBaseService.class);
//                service.putExtra(FireBaseService.Actions.ACTION, FireBaseService.Actions.ADD);
//                service.putExtra("refuel", gson.toJson(o));
//                startService(service);
//            }
        }
        for (int i = 0; i < newList.size(); i++) {
            Intent service = new Intent(MainActivity.this, FireBaseService.class);
            service.putExtra(FireBaseService.Actions.ACTION, FireBaseService.Actions.ADD);
            service.putExtra("station", gson.toJson(newList.get(i)));
            startService(service);
        }
        oldRefuels = tmp;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appDatabase = Room.databaseBuilder(this, AppDatabase.class, "stations").build();

        new AsyncTask<AppDatabase, Void, List<Station>>(){

            @Override
            protected void onPostExecute(List<Station> stations) {
                super.onPostExecute(stations);
                oldStations = stations;
                appDatabase.stationDao().getAll().observe(MainActivity.this, new Observer<List<Station>>() {
                    @Override
                    public void onChanged(List<Station> stations) {
                        compareStations(stations);
                    }
                });
            }

            @Override
            protected List<Station> doInBackground(AppDatabase... appDatabases) {
                return appDatabases[0].stationDao().getList();
            }
        }.execute(appDatabase);

        new AsyncTask<AppDatabase, Void, List<Refuel>>(){

            @Override
            protected void onPostExecute(List<Refuel> refuels) {
                super.onPostExecute(refuels);
                oldRefuels = refuels;
                appDatabase.refuelDao().getAll().observe(MainActivity.this, new Observer<List<Refuel>>() {
                    @Override
                    public void onChanged(List<Refuel> refuels) {
                        compareRefuels(refuels);
                    }
                });
            }

            @Override
            protected List<Refuel> doInBackground(AppDatabase... appDatabases) {
                return appDatabases[0].refuelDao().getList();
            }
        }.execute(appDatabase);

        appDatabase.refuelDao().getAll().observe(this, new Observer<List<Refuel>>() {

            @Override
            public void onChanged(List<Refuel> refuels) {
                for (Refuel refuel : refuels) {
                    Intent service = new Intent(MainActivity.this, FireBaseService.class);
                    service.putExtra(FireBaseService.Actions.ACTION, FireBaseService.Actions.ADD);
                    service.putExtra("refuel", gson.toJson(refuel));
                    startService(service);
                }
            }
        });

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this,
                getSupportFragmentManager(), appDatabase, this);
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        FloatingActionButton fabButton = findViewById(R.id.floatingActionButton);

        fabButton.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            Bundle extras = data.getExtras();
            if (extras != null && requestCode == ADD_RESULT_OK) {
                String stat = extras.getString(STATION);
                final Station s = gson.fromJson(stat, Station.class);
                String ref = extras.getString(REFUEL);
                final Refuel r = gson.fromJson(ref, Refuel.class);
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        StationDao stationDao = appDatabase.stationDao();
                        RefuelDao refuelDao = appDatabase.refuelDao();
                        Station byAddress = stationDao.getByAddress(s.address);
                        if (byAddress == null) {
                            stationDao.insert(s);
                            Station t = stationDao.getByAddress(s.address);
                            r.stationId = t.id;
                            refuelDao.insert(r);
                        } else {
                            r.stationId = byAddress.id;
                            refuelDao.insert(r);
                        }
                    }
                }).start();
            }
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, AddNewRefueling.class);
        startActivityForResult(intent, ADD_RESULT_OK);
    }
}