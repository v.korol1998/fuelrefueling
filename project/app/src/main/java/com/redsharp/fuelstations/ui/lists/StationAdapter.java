package com.redsharp.fuelstations.ui.lists;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.redsharp.fuelstations.R;
import com.redsharp.fuelstations.items.Station;
import com.redsharp.fuelstations.sqlmanagers.AppDatabase;

import java.util.LinkedList;
import java.util.List;

public class StationAdapter extends RecyclerView.Adapter<StationAdapter.StationViewHolder>
    implements ItemTouchHelperAdapter{

    private List<Station> stations = new LinkedList<>();
    private Context context;
    private AppDatabase appDatabase;

    private void setList(List<Station> stations){
        this.stations = stations;
        this.notifyDataSetChanged();
    }

    public StationAdapter(AppDatabase stations, Context context, LifecycleOwner owner) {
//        this.stations = stations;
        appDatabase = stations;
        stations.stationDao().getAll().observe(owner, new Observer<List<Station>>() {
            @Override
            public void onChanged(List<Station> stations) {
                setList(stations);
            }
        });
        new AsyncTask<AppDatabase, Void, List<Station>>(){

            @Override
            protected void onPostExecute(List<Station> stations) {
                setList(stations);
            }

            @Override
            protected List<Station> doInBackground(AppDatabase... appDatabases) {
                return appDatabases[0].stationDao().getList();
            }
        }.execute(stations);

        this.context = context;
    }

    @NonNull
    @Override
    public StationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new StationViewHolder(LayoutInflater.from(context).inflate(R.layout.fuel_station_card, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull StationViewHolder holder, int position) {
        holder.bind(stations.get(position));
    }

    @Override
    public int getItemCount() {
        return stations.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {}

    @Override
    public void onItemDismiss(int position) {
        final Station remove = stations.remove(position);
        new Thread(new Runnable(){
            @Override
            public void run() {
                appDatabase.refuelDao().deleteByStation(remove.id);
                appDatabase.stationDao().delete(remove);
            }
        }).start();
//        appDatabase.stationDao().delete(remove);
        notifyItemRemoved(position);
        Log.d("dismis", "");
    }

    public static class StationViewHolder extends RecyclerView.ViewHolder {

        Station station;

        TextView name;
        TextView address;

        public StationViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            address = itemView.findViewById(R.id.address);
        }

        public void bind(Station station) {
            this.station = station;
            name.setText(station.name);
            address.setText(station.address);
        }
    }
}
