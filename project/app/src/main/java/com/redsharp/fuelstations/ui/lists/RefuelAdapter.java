package com.redsharp.fuelstations.ui.lists;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.redsharp.fuelstations.R;
import com.redsharp.fuelstations.items.Refuel;
import com.redsharp.fuelstations.sqlmanagers.AppDatabase;
import com.redsharp.fuelstations.ui.AddNewRefueling;

import java.util.LinkedList;
import java.util.List;

public class RefuelAdapter extends RecyclerView.Adapter<RefuelAdapter.RefuelViewHolder>
    implements ItemTouchHelperAdapter{

    private List<Refuel> refuels = new LinkedList<>();

    private AppDatabase appDatabase;

    private Context context;

    public RefuelAdapter(AppDatabase refuels, Context context, LifecycleOwner owner) {
        appDatabase = refuels;
        appDatabase.refuelDao().getAll().observe(owner, new Observer<List<Refuel>>() {
            @Override
            public void onChanged(List<Refuel> refuels) {
                setList(refuels);
            }
        });

        new AsyncTask<AppDatabase, Void, List<Refuel>>(){

            @Override
            protected void onPostExecute(List<Refuel> refuels) {
                setList(refuels);
            }

            @Override
            protected List<Refuel> doInBackground(AppDatabase... appDatabases) {
                return appDatabases[0].refuelDao().getList();
            }
        }.execute(refuels);
        this.context = context;
    }

    private void setList(List<Refuel> refuels) {
        RefuelAdapter.this.refuels = refuels;
        RefuelAdapter.this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RefuelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RefuelViewHolder(
                LayoutInflater.from(context).inflate(R.layout.statistic_card,
                        parent, false),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
    }

    @Override
    public void onBindViewHolder(@NonNull RefuelViewHolder holder, int position) {
        holder.bind(refuels.get(position));
    }

    @Override
    public int getItemCount() {
        return refuels.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) { }

    @Override
    public void onItemDismiss(int position) {
        final Refuel remove = refuels.remove(position);
        notifyItemRemoved(position);
        new Thread(new Runnable() {
            @Override
            public void run() {
                appDatabase.refuelDao().delete(remove);
            }
        }).start();
    }

    public static class RefuelViewHolder extends RecyclerView.ViewHolder {

        private TextView type;
        private TextView price;
        private TextView volume;

        public RefuelViewHolder(@NonNull View itemView, View.OnClickListener onClickListener) {
            super(itemView);
            type = itemView.findViewById(R.id.type);
            price = itemView.findViewById(R.id.name);
            volume = itemView.findViewById(R.id.address);
        }

        public void bind(Refuel refuel) {
            type.setText(refuel.type);
            price.setText(String.valueOf(refuel.price));
            volume.setText(String.valueOf(refuel.volume));
        }
    }
}
