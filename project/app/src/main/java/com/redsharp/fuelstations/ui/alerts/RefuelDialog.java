package com.redsharp.fuelstations.ui.alerts;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.textfield.TextInputLayout;
import com.redsharp.fuelstations.R;
import com.redsharp.fuelstations.items.Refuel;
import com.redsharp.fuelstations.items.Station;
import com.redsharp.fuelstations.ui.AddNewRefueling;
import com.redsharp.fuelstations.ui.addmodul.Pusher;

public class RefuelDialog extends DialogFragment implements View.OnClickListener {

    private TextInputLayout address;
    private TextInputLayout name;
    private TextInputLayout volume;
    private TextInputLayout price;
    private TextInputLayout type;

    private static final String DIALOG = "dialog";

    private String addressLine;
    private Pusher pusher;

    public RefuelDialog(String addressLine, Pusher pusher) {
        this.addressLine = addressLine;
        this.pusher = pusher;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.ditails_dialog, container, false);
        address = v.findViewById(R.id.address);
        name = v.findViewById(R.id.name);
        volume = v.findViewById(R.id.volume);
        price = v.findViewById(R.id.price);
        type = v.findViewById(R.id.type);
        address.getEditText().setText(addressLine);

        Button cancel = v.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        Button add = v.findViewById(R.id.add);
        add.setOnClickListener(this);

        Log.d(DIALOG, "create");

        setRetainInstance(true);

        Dialog dialog = getDialog();
        dialog.setTitle("Fill fields");

        if(savedInstanceState != null){
            if (savedInstanceState.containsKey("address"))
                address.getEditText().setText(savedInstanceState.getString("address"));
            if (savedInstanceState.containsKey("name"))
                name.getEditText().setText(savedInstanceState.getString("name"));
            if (savedInstanceState.containsKey("volume"))
                volume.getEditText().setText(savedInstanceState.getString("volume"));
            if (savedInstanceState.containsKey("price"))
                price.getEditText().setText(savedInstanceState.getString("price"));
            if (savedInstanceState.containsKey("type"))
                type.getEditText().setText(savedInstanceState.getString("type"));
        }

        return v;
    }

    @Override
    public void onSaveInstanceState(Bundle out) {
        super.onDetach();
        String addr = address.getEditText().getText().toString();
        if (!addr.equals(""))
            out.putString("address", addr);
        String name = this.name.getEditText().getText().toString();
        if (!name.equals(""))
            out.putString("name", name);
        String pr = price.getEditText().getText().toString();
        if (!pr.equals(""))
            out.putString("price", pr);
        String vol = volume.getEditText().getText().toString();
        if (!vol.equals(""))
            out.putString("volume", vol);
        String ty = type.getEditText().getText().toString();
        if (!ty.equals(""))
            out.putString("type", ty);
    }

    @Override
    public void onClick(View view) {
        String nameText = name.getEditText().getText().toString();
        String priceText = price.getEditText().getText().toString();
        String volumeText = volume.getEditText().getText().toString();
        String typeText = type.getEditText().getText().toString();
        String address = this.address.getEditText().getText().toString();
        if(nameText.equals("") || priceText.equals("") || volumeText.equals("") || typeText.equals("") || address.equals("")){
            return;
        }
        Station s = new Station(nameText, address);
        Refuel r = new Refuel(Double.parseDouble(priceText), Double.parseDouble(volumeText), typeText);
        pusher.push(s, r);
        dismiss();
    }
}
