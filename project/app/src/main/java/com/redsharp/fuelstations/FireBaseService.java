package com.redsharp.fuelstations;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.redsharp.fuelstations.items.Refuel;
import com.redsharp.fuelstations.items.Station;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class FireBaseService extends Service implements Runnable {


    public FireBaseService() {
        this.actions = new LinkedList<>();
    }

    private class ActionStation{
        Station station;
        int action;
    }

    private class ActionRefuel{
        Refuel refuel;
        int action;
    }

    final List<Runnable> actions;
    FirebaseDatabase db;
    Thread thread;

    public interface Actions{
        String ACTION = "action";
        int ADD = 1;
        int DELETE = 2;
        int UPDATE = 3;
    }

    private boolean checkConnection(){
        ConnectivityManager cm =
                (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        String NOTIFICATION_CHANNEL_ID = "com.redsharp.fuelstations";
        FirebaseApp firebaseApp = FirebaseApp.initializeApp(this);
        db = FirebaseDatabase.getInstance(firebaseApp);
        thread = new Thread(this);
        String s = "";
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "test", NotificationManager.IMPORTANCE_HIGH);
            manager.createNotificationChannel(notificationChannel);
        }

        Notification notification = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("FuelStation")
                .setContentText("wait for internet")
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        if(!checkConnection())
            startForeground(1, notification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle extras = intent.getExtras();
        if (extras != null && extras.containsKey(Actions.ACTION)) {
            int action = extras.getInt(Actions.ACTION);
            if (extras.containsKey("refuel")) {
                ActionRefuel actionRefuel = new ActionRefuel();
                actionRefuel.action = action;
                actionRefuel.refuel = new Gson().fromJson(extras.getString("refuel"), Refuel.class);
                synchronized (actions) {
                    actions.add(new RefuelPusher(db, actionRefuel));
                }
            }
            if (extras.containsKey("station")) {
                ActionStation actionStation = new ActionStation();
                actionStation.action = action;
                actionStation.station = new Gson().fromJson(extras.getString("station"), Station.class);
                synchronized (actions) {
                    actions.add(new StationPusher(db, actionStation));
                }
            }
        }
        if(!thread.isAlive()) {
            thread = new Thread(this);
            thread.start();
        }
        return super.onStartCommand(intent, flags, startId);
    }



    @Override
    public void run() {
        while (actions.size() > 0) {
            try {
                if (checkConnection()) {
                    Runnable remove = actions.remove(0);
                    remove.run();
                } else {
                    Thread.sleep(1000);
                }
            } catch (InterruptedException | IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
        stopSelf();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private class StationPusher implements Runnable{

        private FirebaseDatabase db;
        ActionStation actionStation;

        StationPusher(FirebaseDatabase db, ActionStation actionStation){
            this.actionStation = actionStation;
            this.db = db;
        }

        @Override
        public void run() {
            DatabaseReference reference = db.getReference("/stations");

            DatabaseReference child = reference.child(String.valueOf(actionStation.station.id));
            if(actionStation.action == Actions.DELETE){
                child.removeValue();
            } else {
                HashMap<String, Object> update = new HashMap<>();
                update.put("name", actionStation.station.name);
                update.put("address", actionStation.station.address);
                child.updateChildren(update);
            }
        }
    }

    private class RefuelPusher implements Runnable{

        private FirebaseDatabase db;
        ActionRefuel actionRefuel;

        RefuelPusher(FirebaseDatabase db, ActionRefuel actionRefuel){
            this.actionRefuel = actionRefuel;
            this.db = db;
        }

        @Override
        public void run() {
            DatabaseReference reference = db.getReference("/refuels");
            DatabaseReference child = reference.child(String.valueOf( actionRefuel.refuel.id));
            if( actionRefuel.action == Actions.DELETE){
                child.removeValue();
            } else {
                HashMap<String, Object> update = new HashMap<>();
                update.put("price",  actionRefuel.refuel.price);
                update.put("type",  actionRefuel.refuel.type);
                update.put("volume",  actionRefuel.refuel.volume);
                update.put("station id",  actionRefuel.refuel.stationId);
                child.updateChildren(update);
            }
        }
    }
}
