package com.redsharp.fuelstations.items;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity
public class Refuel {

    @PrimaryKey (autoGenerate = true)
    public long id;

    public double price;

    public double volume;

    public String type;

    @ForeignKey(entity = Station.class, parentColumns = {"id"}, childColumns = "stationId")
    public long stationId;

    public Refuel(double price, double volume, String type) {
        this.price = price;
        this.volume = volume;
        this.type = type;
    }
}
