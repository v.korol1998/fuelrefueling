package com.redsharp.fuelstations.sqlmanagers;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.redsharp.fuelstations.items.Refuel;
import com.redsharp.fuelstations.items.Station;

@Database(entities = {Station.class, Refuel.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract RefuelDao refuelDao();
    public abstract StationDao stationDao();
}
