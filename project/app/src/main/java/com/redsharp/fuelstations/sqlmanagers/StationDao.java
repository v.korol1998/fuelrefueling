package com.redsharp.fuelstations.sqlmanagers;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.redsharp.fuelstations.items.Station;

import java.util.List;

@Dao
public interface StationDao {

    @Query("SELECT * FROM Station")
    LiveData<List<Station>> getAll();

    @Query("SELECT * FROM Station")
    List<Station> getList();

    @Query("SELECT * FROM Station WHERE id = :id")
    Station getById(long id);

    @Query("SELECT * FROM Station WHERE address = :address")
    Station getByAddress(String address);

    @Insert
    void insert(Station employee);

    @Update
    void update(Station employee);

    @Delete
    void delete(Station employee);

}
