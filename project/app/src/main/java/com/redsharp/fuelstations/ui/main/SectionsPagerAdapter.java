package com.redsharp.fuelstations.ui.main;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.LifecycleOwner;

import com.redsharp.fuelstations.R;
import com.redsharp.fuelstations.items.Refuel;
import com.redsharp.fuelstations.items.Station;
import com.redsharp.fuelstations.sqlmanagers.AppDatabase;
import com.redsharp.fuelstations.ui.lists.RecyclerFragment;
import com.redsharp.fuelstations.ui.lists.RefuelAdapter;
import com.redsharp.fuelstations.ui.lists.StationAdapter;

import java.security.acl.Owner;
import java.util.LinkedList;
import java.util.List;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.fuel_stations, R.string.statistic};
    private final Context mContext;
    private final AppDatabase appDatabase;

    private Fragment[] fragments = new Fragment[2];

    public SectionsPagerAdapter(Context context, FragmentManager fm, final AppDatabase appDatabase,
                                LifecycleOwner owner) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mContext = context;
        this.appDatabase = appDatabase;
        fragments[0] = new RecyclerFragment(new StationAdapter(appDatabase, context, owner));
        fragments[1] = new RecyclerFragment(new RefuelAdapter(appDatabase, context, owner));
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 2;
    }
}