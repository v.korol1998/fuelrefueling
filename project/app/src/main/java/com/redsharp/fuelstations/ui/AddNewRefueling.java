package com.redsharp.fuelstations.ui;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.PlaceLikelihood;
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest;
import com.google.android.libraries.places.api.net.FindCurrentPlaceResponse;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.redsharp.fuelstations.MainActivity;
import com.redsharp.fuelstations.R;
import com.redsharp.fuelstations.items.Refuel;
import com.redsharp.fuelstations.items.Station;
import com.redsharp.fuelstations.ui.addmodul.Pusher;
import com.redsharp.fuelstations.ui.alerts.RefuelDialog;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class AddNewRefueling extends FragmentActivity implements OnMapReadyCallback,
        LocationListener, GoogleMap.OnMapClickListener, Pusher {

    private static final String TAG = "addnew";
    private GoogleMap mMap;
    private int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private LocationManager locationManager;
    private long MIN_TIME = 90;
    private float MIN_DISTANCE = 100;

    private TextInputLayout name;
    private TextInputLayout volume;
    private TextInputLayout price;
    private TextInputLayout type;

    AlertDialog alertDialog;

    FragmentManager supportFragmentManager;

    private String address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportFragmentManager = getSupportFragmentManager();
        setContentView(R.layout.activity_add_new_refueling);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        getLocationPermission();
        mMap.setOnMapClickListener(this);
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
    }


    private void showMyLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showMyLocation();
            }
        }
    }

    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            showMyLocation();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if(mMap != null){
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15f);
            mMap.moveCamera(cameraUpdate);
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) { }

    @Override
    public void onProviderEnabled(String s) { }

    @Override
    public void onProviderDisabled(String s) { }

    @Override
    public void onMapClick(LatLng latLng) {
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(latLng));
        Geocoder geocoder = new Geocoder(AddNewRefueling.this, Locale.getDefault());
        try {
            List<Address> fromLocation = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if(fromLocation.size() >= 1) {
                address = fromLocation.get(0).getAddressLine(0);
                showAlertDialog(address);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showAlertDialog(String addressLine){
        RefuelDialog refuelDialog = new RefuelDialog(addressLine, this);
        refuelDialog.showNow(supportFragmentManager, "dialog");
    }

    @Override
    public void push(Station station, Refuel refuel) {
        Intent resIntent = new Intent();
        Gson gson = new Gson();
        resIntent.putExtra(MainActivity.STATION, gson.toJson(station));
        resIntent.putExtra(MainActivity.REFUEL, gson.toJson(refuel));
        setResult(MainActivity.ADD_RESULT_OK, resIntent);
        finish();
    }
}
